
#include <Wire.h>
#include <RTC.h>
int Relay = 7;
static DS3231 RTC;
int offHour = 6;
int onHour = 18;
int offMin = 0;
int onMin = 0;

int offMinAdj = 10;

void setup()
{
	Serial.begin(9600);
	RTC.begin();
  pinMode(Relay, OUTPUT);
  digitalWrite(Relay, HIGH);
	//RTC.stopClock();
//  RTC.setEpoch(1626478140);
   RTC.setHourMode(CLOCK_H24);


}

void loop()
{
 switch (RTC.getMonth() ) {
  case 1:
     offHour = 6;
    onHour = 17;
    offMin = 39;
    onMin = 13;
    break;
  case 2:
     offHour = 6;
    onHour = 17;
    offMin = 21;
    onMin = 35;
    break;
      case 3:
     offHour = 6;
    onHour = 18;
    offMin = 00;
    onMin = 02;
    break;
      case 4:
     offHour = 5;
    onHour = 18;
    offMin = 35;
    onMin = 10;
    break;
      case 5:
        offHour = 5;
    onHour = 18;
    offMin = 16;
    onMin = 23;
    break;
   
  case 6:
        offHour = 5;
    onHour = 18;
    offMin = 11;
    onMin = 36;
    break;
    
    case 7:
        offHour = 5;
    onHour = 18;
    offMin = 20;
    onMin = 38;

//    offHour = 5;
//    onHour = 23;
//    offMin = 20;
//    onMin = 32;
    break;
    
      case 8:
         offHour = 5;
    onHour = 18;
    offMin = 33;
    onMin = 21;
    break;
      case 9:
         offHour = 5;
    onHour = 17;
    offMin = 44;
    onMin = 52;
    break;
      case 10:
         offHour = 05;
    onHour = 17;
    offMin = 55;
    onMin = 22;
    break;
          case 11:
         offHour = 6;
    onHour = 17;
    offMin = 12;
    onMin = 02;
    break;
          case 12:
         offHour = 6;
    onHour = 17;
    offMin = 32;
    onMin = 03;
    break;
  default:
        offHour = 5;
    onHour = 18;
    offMin = 30;
    onMin = 0;
    break;
}
//  types(RTC.getMinutes());
Serial.print("Off Hour: ");
Serial.print(offHour);
Serial.print(" Off Min: ");
Serial.println( offMin);

printClock();

if(RTC.getHours()>= offHour   && RTC.getHours()<= onHour ){
  if(RTC.getHours()== onHour){
    if(RTC.getMinutes() >= onMin){
              digitalWrite(Relay, LOW);
    Serial.println("Relay LOW || ON IF MIN");
    }else{
              digitalWrite(Relay, HIGH);
    Serial.println("Relay HIGH || ON ELSE MIN");
    }

  }else{
        digitalWrite(Relay, LOW);
    Serial.println("Relay LOW || ON ELSE");
  }

  }else{
    if(RTC.getHours() ==  offHour){
      if(RTC.getMinutes()>= offMin){
                      digitalWrite(Relay, LOW);
    Serial.println("Relay LOW || OFF MIN IF");
      }else{
                      digitalWrite(Relay, HIGH);
    Serial.println("Relay HIGH || OFF ELSE MIN");
      }
    }else{
              digitalWrite(Relay, HIGH);
    Serial.println("Relay HIGH || OFF ELSE");
    }

  }


   delay (1000);



}

void printClock(){
  Serial.print("Is Clock Running: ");
  if (RTC.isRunning())
  {
    Serial.println("Yes");
    Serial.print(RTC.getDay());
    Serial.print("-");
    Serial.print(RTC.getMonth());
    Serial.print("-");
    Serial.print(RTC.getYear());
    Serial.print(" ");
    Serial.print(RTC.getHours());
    Serial.print(":");
    Serial.print(RTC.getMinutes());
    Serial.print(":");
    Serial.print(RTC.getSeconds());
    Serial.print("");
    if (RTC.getHourMode() == CLOCK_H12)
    {
      switch (RTC.getMeridiem()) {
      case HOUR_AM:
        Serial.print(" AM");
        break;
      case HOUR_PM:
        Serial.print(" PM");
        break;
      }
    }
    Serial.println("");
  delay(1000);
  }
}

//void types(String a) { Serial.println("it's a String"); }
//void types(int a) { Serial.println("it's an int"); }
//void types(char *a) { Serial.println("it's a char*"); }
//void types(float a) { Serial.println("it's a float"); }
//void types(bool a) { Serial.println("it's a bool"); }
